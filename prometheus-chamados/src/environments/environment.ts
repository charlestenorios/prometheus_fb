// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB6dUXo1woeHZ8pN6vCT2k3pi9RgMZP9Ws",
    authDomain: "prometheus-chamado.firebaseapp.com",
    projectId: "prometheus-chamado",
    storageBucket: "prometheus-chamado.appspot.com",
    messagingSenderId: "761729138458",
    appId: "1:761729138458:web:0e5431768af59079b3f6a3",
    measurementId: "G-VNMKDSE2N3"

    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
