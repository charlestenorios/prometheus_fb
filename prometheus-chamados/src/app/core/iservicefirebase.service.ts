import {Model} from '../models/model';
import{ICrud} from './icrud.interface';

export abstract class ServiceFirebase<T extends Model> implements ICrud<T> {}
