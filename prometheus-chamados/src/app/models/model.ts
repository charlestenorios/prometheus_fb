import {classToPlain} from "class-transformer";

/*
 O Firebase só permite a manipulação de objetos JavaScript
desserializados (salvar, atualizar), também chamados de objetos literais
*/
export abstract class Model {
  id:string;
  atCriacao: Date;

  toObject(): object {
    let obj :any = classToPlain(this); // realiza desserialização para uma classe Literal,
    delete obj.id;
    return obj;
  }
}
