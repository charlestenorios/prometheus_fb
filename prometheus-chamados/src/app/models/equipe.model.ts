import {Model} from '../models/model';
import {Secretaria} from './secretaria.model';
import {Funcionario} from './funcionario.model';


export class Equipe extends Model{
  nomeEquipe: string;
  nomeResponavel: string;
  foneResponsavel: string;
  statusEquipe: string;
  funcinario: Funcionario[];

}
