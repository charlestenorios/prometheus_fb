import {Model} from '../models/model';
import {Secretaria} from './secretaria.model';


export class Funcionario extends Model{
  nome: string;
  funcao: string;
  secretaria: Secretaria;
}
