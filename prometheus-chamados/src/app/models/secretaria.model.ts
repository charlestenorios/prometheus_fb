import {Model} from '../models/model';
import{Prefeitura} from './prefeitura.model';

export class Secretaria {
  nomeSecretaria: string;
  foneSecretiaria: string;
  nomeScretario : string;
  prefeitura : Prefeitura;
}
