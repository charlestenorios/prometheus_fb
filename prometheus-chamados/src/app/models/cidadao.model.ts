import {Model} from '../models/model';
import {Prefeitura} from './prefeitura.model';

export class Cidadao extends Model{
  nome: string;
  sexo: string;
  endreco: string;
  bairro: string;
  prefeitura: Prefeitura;

}
