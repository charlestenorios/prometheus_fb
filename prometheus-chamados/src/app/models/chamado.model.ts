import {Model} from '../models/model';
import {Secretaria} from './secretaria.model';
import {Cidadao} from './cidadao.model';


export class Chamado extends Model {
  solicitante: Cidadao;
  dataAbertura: any;
  ultimaAtualizacao: any;
  descricao: string;
  status: string;
  destino: Secretaria;

}


