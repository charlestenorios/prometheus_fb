import {Model} from '../models/model';


export class Prefeitura extends Model{
  nome: string;
  cnpj: string;
  telefone?: string;
}
