import {Model} from '../models/model';
import {Secretaria} from './secretaria.model';
import {Chamado} from './chamado.model';
import{Equipe} from './equipe.model';


export class Ordemservico extends Model{
  secretaria: Secretaria;
  chamado: Chamado[];
  equipe: Equipe[];
  movimentacoes: Movimentacao[];
  statusOs: string;
  aberturaOS: Date;

}

export class Movimentacao extends Model {
  secretaria: Secretaria;
  dataHora: Date;
  status: string
  descricao: string;
}

